# Manuscriptum

Manuscriptum Windows client.

## What is it?

Unnoficial open source copliment for brilliant [Supernote®](https://supernote.com/) tablet.

Open yours notes on Windows from filesystem.

It supports:
- X series file format.
> Algorithm is a result of reverse enginiering and may have problems. If your note has nobeen rendered correctly - open issue.
- Navigation with titles.
- Navigation with links.
- Dark/Light mode.

## Getting Started

1. Add new directory by clicking on `+ Local Directory`. And point it to `Supernote` root directory. 
> *Important* navigation will work only if `Supernote` folder has been chosel as the root.
2. Navigate through your notes file, choose one and enjoy.

## Some screenshots
![Dark and Ligth Themes](screenshots/dark-ligth.png)

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/yamsergeyb)
