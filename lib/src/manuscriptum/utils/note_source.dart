import 'package:ratta_supernote_file_widget/adapter/file/_note_source.dart';

extension InitialiseSourceBuilder on NoteSource {
  Future<NoteSource> build() async {
    await initialise();
    return this;
  }
}