extension ObjectAdapter<T> on T? {
  O? adapt<O>(O? Function(T) adapter, [O Function()? fallback]) {
    final checked = this;
    if (checked != null) {
      return adapter(checked);
    } else {
      return fallback?.call();
    }
  }
}