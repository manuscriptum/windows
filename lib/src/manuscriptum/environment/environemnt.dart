import 'package:environment/environment.dart';
import 'package:get_storage/get_storage.dart';
import 'package:manuscriptum/src/manuscriptum/environment/installers.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

class DesktopEnvironment extends Environment {
  late final ConfigurationsStorage _configurations;
  late final InstallersList _installers = DesktopFilesystemsInstallers();
  late final FileSystem _localFileSystem;

  DesktopEnvironment._({required GetStorage storage, required FileSystem local}) {
    _configurations = ConfigurationsStorage(storage);
    _localFileSystem = local;
  }

  static Future<DesktopEnvironment> create() async {
    final storage = GetStorage();
    await storage.initStorage;
    final local = await LocalFileSystemInstaller().install();
    return DesktopEnvironment._(storage: storage, local: local);
  }

  @override
  ConfigurationsStorage get configurations => _configurations;

  @override
  InstallersList get installers => _installers;

  @override
  FileSystem get localFilesystem => _localFileSystem;

}