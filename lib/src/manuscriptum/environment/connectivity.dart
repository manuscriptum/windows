import 'package:remote_filesystem/remote_filesystem.dart';

class AlwaysOnConnectivityProvider extends ConnectivityProvider {
  @override
  Future<int> connectionState() async => ConnectivityState.wifi;

  @override
  int connectionStateSync() => ConnectivityState.wifi;
}
