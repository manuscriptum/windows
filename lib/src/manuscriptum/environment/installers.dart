import 'package:environment/environment.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

class DesktopFilesystemsInstallers extends InstallersList {
  final Map<String, FileSystemInstaller> _installers = {
    'local': LocalFileSystemInstaller()
  };

  @override
  FileSystemInstaller<FileSystem>? operator [](String? id) {
    if (id == null) {
      return null;
    } else {
      return _installers[id];
    }
  }

  @override
  Map<String, FileSystemInstaller<FileSystem>> get entries => _installers;

  @override
  String? idOf(FileSystemInstaller<FileSystem>? installer) =>
      _installers.entries
          .firstWhere(
              (element) => installer?.runtimeType == element.value.runtimeType)
          .key;

  @override
  List<FileSystemInstaller<FileSystem>> get values =>
      _installers.values.toList();
}
