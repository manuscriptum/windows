import 'package:environment/environment.dart';
import 'package:file_picker/file_picker.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

class InstallerConfiguration {
  final Environment environment;

  InstallerConfiguration({required this.environment});

  Future<SourceConfiguration?> configure(FileSystemInstaller installer) async {
    if (installer is LocalFileSystemInstaller) {
      String? root = await FilePicker.platform.getDirectoryPath();
      var installerId = environment.installers.idOf(installer);
      if (root != null && installerId != null) {
        final configuration = SourceConfiguration(
            installerId: installerId,
            root: root,
            title: 'Local Directory',
            avatar: '',
            id: '${DateTime.now().millisecondsSinceEpoch}'
        );
        environment.configurations.add(configuration);
        return configuration;
      } else {
        return null;
      }
    } else {
      throw ArgumentError('$installer is not yet supported.');
    }
  }
}