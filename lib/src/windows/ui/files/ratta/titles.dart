import 'package:environment/environment.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:ratta_supernote_file_widget/adapter/file/_note_source.dart';
import 'package:ratta_supernote_file_widget/color/_inverter.dart';
import 'package:ratta_supernote_file_widget/model/model.dart' as ratta;

_pageNumber(String href) => Uri.parse(href).queryParameters['page'];

class RattaTitlesList extends StatefulWidget {
  final Environment environment;
  final NoteSource source;
  final Function(int)? listener;
  final ScrollController? controller;

  const RattaTitlesList(
      {super.key,
      required this.environment,
      required this.source,
      this.listener,
      this.controller});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<RattaTitlesList> {
  List<ratta.Title>? _titles;

  @override
  void initState() {
    super.initState();
    widget.source.titles.listen((event) {
      setState(() {
        _titles = event;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final titles = _titles;

    if (titles == null) {
      return const SizedBox.shrink();
    } else {
      return ListView.builder(
          controller: widget.controller,
          padding: const EdgeInsets.all(5.0),
          physics: const ClampingScrollPhysics(),
          itemCount: titles.length,
          itemBuilder: (context, index) {
            var imageMd5 = titles[index].md5;
            if (imageMd5.isEmpty) {
              return const SizedBox.shrink();
            }
            return ListTile(
                onPressed: () {
                  var argument = _pageNumber(titles[index].link.href);
                  if (argument != null) {
                    final page = int.parse(argument);
                    widget.listener?.call(page);
                  }
                },
                title: Column(children: [
                  SizedBox(
                      height: 30,
                      child: ColorInverter(
                          shouldInvert: FluentTheme.of(context).brightness ==
                              Brightness.dark,
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Image.file(widget.environment
                                  .localFilesystem.systemTempDirectory
                                  .childFile('$imageMd5.png'))))),
                  const Divider()
                ]));
          });
    }
  }
}
