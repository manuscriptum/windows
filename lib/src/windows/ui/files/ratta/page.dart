import 'package:environment/environment.dart';
import 'package:file_system_voyager/utils/_snapshot.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';
import 'package:manuscriptum/src/windows/ui/files/ratta/pager.dart';
import 'package:manuscriptum/src/windows/ui/sidebar/sidebar.dart';
import 'package:manuscriptum/src/windows/ui/voyager/voyager.dart';
import 'package:manuscriptum/src/windows/ui/window/scaffold.dart';
import 'package:manuscriptum/src/windows/ui/window/window.dart';
import 'package:manuscriptum/src/manuscriptum/utils/note_source.dart';
import 'package:ratta_supernote_file_widget/adapter/file/_note_source.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

import '../../sidebar/ratta.dart';

typedef _Setup = (FileSystem filesystem, NoteSource source);

class RattaFilePage extends StatelessWidget {
  final Environment environment;
  final SourceConfiguration configuration;
  final String path;
  final String syntheticRoot;
  final String destination;
  final String? pageId;

  const RattaFilePage(
      {super.key,
      required this.environment,
      required this.configuration,
      required this.path,
      required this.syntheticRoot,
      required this.destination,
      this.pageId});

  @override
  Widget build(BuildContext _) => FutureBuilder(
      future: Future(() async {
        final filesystem =
            await environment.installers[configuration.installerId]!.install();
        final source = await NoteSource(
                cacheDirectory: environment.localFilesystem.systemTempDirectory,
                noteFile: filesystem.file(path))
            .build();
        return (filesystem, source);
      }),
      builder: (_, snapshot) =>
          snapshot.handle(onDone: _handle, defaultWidget: _default()));

  Widget _handle(BuildContext context, _Setup data) {
    final controller = PageController(initialPage: data.$2.lastOperationPage);

    return ManuscriptumWindow.create(
        context: context,
        environment: environment,
        configuration: configuration,
        content: ManuscriptumScaffold(
            start: WindowsFileVoyager(
                syntheticRoot: syntheticRoot,
                destination: destination,
                configuration: configuration,
                installer: environment.installers[configuration.installerId]!),
            content: RattaFilePager(
                source: data.$2,
                environment: environment,
                configuration: configuration,
                controller: controller,
                linkListener: (href) {
                  context.push('$syntheticRoot$href');
                }),
            end: RattaNoteSidebar(
                source: data.$2,
                environment: environment,
                listener: (page) => controller.jumpToPage(page - 1))));
  }

  Widget _default() {
    return Builder(builder: (context) {
      return ManuscriptumWindow.create(
        context: context,
        environment: environment,
        configuration: configuration,
      );
    });
  }
}
