import 'dart:async';

import 'package:environment/environment.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/widgets.dart';
import 'package:ratta_supernote_file_widget/adapter/file/_note_source.dart';
import 'package:ratta_supernote_file_widget/model/model.dart' as ratta;
import 'package:ratta_supernote_file_widget/ratta_supernote_file_widget.dart';

class RattaFilePager extends StatefulWidget {
  final NoteSource source;
  final PageController? controller;
  final Environment environment;
  final SourceConfiguration configuration;
  final Function(String href)? linkListener;
  final String? pageId;

  const RattaFilePager(
      {super.key,
      required this.source,
      this.controller,
      required this.environment,
      required this.configuration,
      this.linkListener,
      this.pageId});

  @override
  State<StatefulWidget> createState() => _RattaFilePagerState();
}

class _RattaFilePagerState extends State<RattaFilePager> {
  late final PageController pageController;
  final List<StreamSubscription> subscriptions = [];
  late final List<ratta.Page> pages = [];
  late bool initialPageSet;

  @override
  void initState() {
    super.initState();
    initialPageSet = widget.pageId == null;
    pageController = widget.controller ?? PageController();
    subscriptions.add(widget.source.pages.listen((event) {
      setState(() {
        pages.clear();
        pages.addAll(event);
        if (widget.pageId != null) {
          final selectedPage =
              pages.indexed.where((e) => e.$2.id == widget.pageId).firstOrNull;
          if (selectedPage != null) {
            pageController.jumpToPage(selectedPage.$1);
          }
        }
      });
    }));
  }

  @override
  void dispose() {
    super.dispose();
    for (var subscription in subscriptions) {
      subscription.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (pages.isEmpty) {
      return const SizedBox.shrink();
    }
    var pager = material.Material(
        type: material.MaterialType.transparency,
        child: RattaNote.pager(
            onLinkPressed: (href) {
              var requestedHref = '${widget.configuration.root}$href';
              if (requestedHref.startsWith(widget.source.noteFile.path)) {
                final uri = Uri.parse(href);
                final pageId = uri.queryParameters['pageId'];
                if (pageId != null) {
                  final pageNumber = pages
                      .where((page) => page.id == pageId)
                      .firstOrNull
                      ?.number;
                  if (pageNumber != null) {
                    pageController.jumpToPage(pageNumber);
                  }
                }
              } else {
                widget.linkListener?.call(requestedHref);
              }
            },
            controller: pageController,
            invert: FluentTheme.of(context).brightness == Brightness.dark,
            pages: pages,
            root: widget.environment.localFilesystem.systemTempDirectory,
            hoverColor: FluentTheme.of(context).cardColor));
    return Column(children: <Widget>[
      Row(
        children: [
          IconButton(
              onPressed: () => pageController.previousPage(
                  duration: const Duration(milliseconds: 400),
                  curve: Curves.easeIn),
              icon: const Icon(FluentIcons.back)),
          IconButton(
              onPressed: () => pageController.nextPage(
                  duration: const Duration(milliseconds: 400),
                  curve: Curves.easeOut),
              icon: const Icon(FluentIcons.forward))
        ],
      ),
      Expanded(child: pager)
    ]);
  }
}
