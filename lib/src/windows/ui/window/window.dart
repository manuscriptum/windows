import 'package:environment/environment.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:manuscriptum/src/windows/ui/sidebar/sidebar.dart';

class ManuscriptumWindow extends NavigationView {
  const ManuscriptumWindow({
    super.key,
    super.appBar,
    super.pane,
    super.content,
    super.clipBehavior = Clip.antiAlias,
    super.contentShape,
    super.onOpenSearch,
    super.transitionBuilder,
    super.paneBodyBuilder,
  });

  factory ManuscriptumWindow.create(
      {required BuildContext context,
      SourceConfiguration? configuration,
      Widget? content,
      required Environment environment}) {
    return ManuscriptumWindow(
      appBar: const NavigationAppBar(),
      pane: ManuscriptumSidebar.witRouting(
          context: context,
          environment: environment,
          configuration: configuration,
          content: content),
    );
  }
}
