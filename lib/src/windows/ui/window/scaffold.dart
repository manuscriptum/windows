import 'package:fluent_ui/fluent_ui.dart';
import 'package:manuscriptum/src/manuscriptum/utils/adapter.dart';
import 'package:resizable_widget/resizable_widget.dart';

class ManuscriptumScaffold extends StatelessWidget {
  final Widget? start;
  final Widget? top;
  final Widget content;
  final Widget? end;

  const ManuscriptumScaffold(
      {super.key, this.start, this.top, required this.content, this.end});

  @override
  Widget build(BuildContext context) {
    final container = <Widget>[];
    if (top != null) {
      container.add(top!);
    }
    final row = ResizableWidget(
      percentages: const [0.2, 0.6, 0.2],
      separatorColor: FluentTheme.of(context).borderInputColor,
      separatorSize: 1,
      children: <Widget>[
        start.adapt((e) => e, () => const SizedBox.shrink())!,
        content,
        end.adapt((e) => e, () => const SizedBox.shrink())!,
      ],
    );
    if (top != null) {
      return ResizableWidget(
          isHorizontalSeparator: false, children: [top!, row]);
    } else {
      return row;
    }
  }
}
