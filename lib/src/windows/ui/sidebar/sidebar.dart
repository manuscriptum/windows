import 'package:environment/environment.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:go_router/go_router.dart';
import 'package:manuscriptum/src/manuscriptum/utils/adapter.dart';

import '../../usecase/installer_configuration.dart';
import 'items.dart';

typedef ItemClickListener = Function(
    BuildContext context, SidebarItemValue item);

class ManuscriptumSidebar extends NavigationPane {
  ManuscriptumSidebar({
    super.key,
    super.selected,
    super.onChanged,
    super.size,
    super.header,
    super.items = const [],
    super.footerItems = const [],
    super.autoSuggestBox,
    super.autoSuggestBoxReplacement,
    super.displayMode = PaneDisplayMode.auto,
    super.customPane,
    super.menuButton,
    super.scrollController,
    super.scrollBehavior,
    super.leading,
    super.indicator = const StickyNavigationIndicator(),
  });

  factory ManuscriptumSidebar.witRouting(
      {required Environment environment,
      SourceConfiguration? configuration,
      Widget? content,
      required BuildContext context}) {
    InstallerConfiguration installerConfiguration =
        InstallerConfiguration(environment: environment);
    return ManuscriptumSidebar.environment(
        selected: configuration.adapt((config) => ConfigurationItem(config)),
        environment: environment,
        content: content,
        context: context,
        listener: (context, input) {
          final item = input;
          switch (item) {
            case InstallerItem():
              installerConfiguration
                  .configure(item.value)
                  .then((configuration) {
                if (configuration != null) {
                  context.push('/configuration/${configuration.id}');
                }
              });
              break;
            case ConfigurationItem():
              context.push('/configuration/${item.value.id}');
              break;
          }
        });
  }

  factory ManuscriptumSidebar.environment(
      {required Environment environment,
      SidebarItemValue? selected,
      ItemClickListener? listener,
      required BuildContext context,
      Widget? content}) {
    var items = <SidebarItemValue>[
      ...environment.installers.values.map((e) => InstallerItem(e)).toList(),
      ...environment.configurations
          .all()
          .map((e) => ConfigurationItem(e))
          .toList()
    ];

    var currentIndex =
        items.indexed.where((item) => item.$2 == selected).firstOrNull?.$1 ?? 0;

    var transformedItems = items
        .map((e) => e.sidebar(content, (item) {
              listener?.call(context, item);
            }))
        .toList();
    return ManuscriptumSidebar(
        key: ValueKey(selected ?? ''),
        items: <NavigationPaneItem>[...transformedItems],
        selected: currentIndex);
  }
}
