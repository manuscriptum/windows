import 'package:environment/environment.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

sealed class SidebarItemValue<T> {
  final T value;

  SidebarItemValue(this.value);

  @override
  bool operator ==(Object other) {
    if (other is SidebarItemValue<T>) {
      return this.value == other.value;
    } else {
      return false;
    }
  }

  @override
  int get hashCode => value.hashCode;
}

class InstallerItem extends SidebarItemValue<FileSystemInstaller> {
  InstallerItem(super.value);
}

class ConfigurationItem extends SidebarItemValue<SourceConfiguration> {
  ConfigurationItem(super.value);
}

extension WidgetFactoryForItem on SidebarItemValue {
  PaneItem sidebar([Widget? content, void Function(SidebarItemValue item)? tapOnInstaller]) {
    final item = this;
    return switch (item) {
      InstallerItem() => PaneItem(
          onTap: () {
            tapOnInstaller?.call(item);
          },
          icon: const Icon(FluentIcons.add),
          title: Text(item.value.title),
          body: const SizedBox.shrink()),
      ConfigurationItem() => PaneItem(
          onTap: () {
            tapOnInstaller?.call(item);
          },
          icon: const Icon(FluentIcons.folder),
          title: Text(Uri.parse(item.value.root)
              .pathSegments
              .reversed
              .indexed
              .where((e) => e.$1 == 0 || e.$1 == 1)
              .map((e) => e.$2)
              .toList()
              .reversed
              .join('/')),
          body: Builder(builder: (_) => content ?? const SizedBox.shrink()))
    };
  }
}

extension _InstallerTitle on FileSystemInstaller {
  String get title {
    if (this is LocalFileSystemInstaller) {
      return 'Local directory';
    } else {
      return 'Unknown';
    }
  }
}
