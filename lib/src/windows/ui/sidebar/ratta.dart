import 'package:environment/environment.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:manuscriptum/src/windows/ui/files/ratta/titles.dart';
import 'package:ratta_supernote_file_widget/adapter/file/_note_source.dart';

class RattaNoteSidebar extends StatefulWidget {
  final NoteSource source;
  final Environment environment;
  final Function(int)? listener;
  final ScrollController? controller;

  const RattaNoteSidebar(
      {super.key,
      required this.source,
      required this.environment,
      this.listener,
      this.controller});

  @override
  State<RattaNoteSidebar> createState() => _RattaNoteSidebarState();
}

class _RattaNoteSidebarState extends State<RattaNoteSidebar> {
  late final ScrollController controller;

  @override
  void initState() {
    super.initState();
    controller = widget.controller ?? ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return RattaTitlesList(
        environment: widget.environment,
        source: widget.source,
        listener: widget.listener,
        controller: controller);
  }
}
