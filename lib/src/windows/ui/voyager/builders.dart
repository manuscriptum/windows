import 'package:fluent_ui/fluent_ui.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

Widget directoryBuilder(BuildContext context, Directory directory) {
  return Container(
      padding: const EdgeInsets.all(5),
      child: Row(children: [
        const Icon(FluentIcons.folder_open),
        const SizedBox(width: 10),
        Expanded(
            child: Text(
          directory.basename,
          style: FluentTheme.of(context).typography.body,
        ))
      ]));
}

Widget fileBuilder(BuildContext context, File file) {
  if (!file.basename.endsWith('.note')) {
    return const SizedBox.shrink();
  }
  return Container(
      padding: const EdgeInsets.all(5),
      child: Row(children: [
        const Icon(FluentIcons.document),
        const SizedBox(width: 10),
        Expanded(
            child: Text(
          file.basename,
          style: FluentTheme.of(context).typography.body,
        ))
      ]));
}
