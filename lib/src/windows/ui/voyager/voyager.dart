import 'package:environment/environment.dart';
import 'package:file_system_voyager/file_system_voyager.dart';
import 'package:file_system_voyager/utils/_snapshot.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:manuscriptum/src/manuscriptum/environment/connectivity.dart';
import 'package:remote_filesystem/remote_filesystem.dart';

import 'builders.dart';

class WindowsFileVoyager extends StatelessWidget {
  final String syntheticRoot;
  final String destination;
  final SourceConfiguration configuration;
  final FileSystemInstaller installer;

  const WindowsFileVoyager(
      {super.key,
      required this.syntheticRoot,
      required this.destination,
      required this.configuration,
      required this.installer});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: installer.install(),
        builder: (context, snapshot) {
          return snapshot.handle(
              onDone: (context, filesystem) {
                filesystem.currentDirectory = destination;
                // return ListView.builder(itemCount: 5, itemBuilder: (context, index) => Text('$index'));
                return Material(
                    type: MaterialType.transparency,
                    child: FileSystemVoyager(
                      allowedConnectivity: AllowedConnectivity.any(),
                      connectivityProvider: AlwaysOnConnectivityProvider(),
                      fileSystem: filesystem,
                      fileBuilder: fileBuilder,
                      directoryBuilder: directoryBuilder,
                      root: configuration.root,
                      onChangeDirectory: (path) =>
                          context.push(syntheticRoot + path),
                      onSelection: (file) =>
                          context.push(syntheticRoot + file.path),
                    ));
              },
              defaultWidget: const Center(child: ProgressBar()));
        });
  }
}
