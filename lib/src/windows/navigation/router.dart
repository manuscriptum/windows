import 'package:environment/environment.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:manuscriptum/src/windows/ui/voyager/voyager.dart';
import 'package:manuscriptum/src/windows/ui/window/scaffold.dart';
import 'package:manuscriptum/src/windows/ui/window/window.dart';
import 'package:manuscriptum/src/windows/usecase/installer_configuration.dart';
import 'package:ratta_supernote_file_widget/ratta_supernote_file_widget.dart';

import '../ui/files/ratta/page.dart';

class Routing {
  final Environment environment;
  final RattaFileAdapter rattaFileAdapter;

  final InstallerConfiguration installerConfiguration;

  Routing({required this.environment, required this.rattaFileAdapter})
      : installerConfiguration =
            InstallerConfiguration(environment: environment);

  late final router =
      GoRouter(debugLogDiagnostics: true, initialLocation: '/', routes: [
    GoRoute(
        path: '/',
        pageBuilder: (context, state) {
          if (state.location != GoRouter.of(context).location) {
            return const NoTransitionPage(child: SizedBox.shrink());
          }
          return NoTransitionPage(
              child: Builder(
                  builder: (context) => ManuscriptumWindow.create(
                      context: context,
                      environment: environment,
                      content: const ManuscriptumScaffold(
                          content: SizedBox.shrink()))));
        }),
    ShellRoute(routes: [
      GoRoute(
          path: '/configuration/:id/:path(.*)/:file.note',
          pageBuilder: (context, state) {
            if (state.location != GoRouter.of(context).location) {
              return const NoTransitionPage(child: SizedBox.shrink());
            }
            final id = state.pathParameters['id']!;
            final destination = "${state.pathParameters['path']}";
            final file = "$destination/${state.pathParameters['file']}.note";
            final configuration = environment.configurations.findById(id);
            final syntheticRoot = '/configuration/$id/';
            final pageId = state.pathParameters['pageId'];

            return NoTransitionPage(
                child: RattaFilePage(
                    environment: environment,
                    configuration: configuration,
                    path: file,
                    destination: destination,
                    syntheticRoot: syntheticRoot,
                    pageId: pageId));
          }),
      GoRoute(
          path: "/configuration/:id",
          pageBuilder: (context, state) {
            if (state.location != GoRouter.of(context).location) {
              return const NoTransitionPage(child: SizedBox.shrink());
            }
            final id = state.pathParameters['id']!;
            final configuration = environment.configurations.findById(id);
            final installer =
                environment.installers[configuration.installerId]!;

            return NoTransitionPage(
                child: ManuscriptumWindow.create(
                    context: context,
                    configuration: configuration,
                    environment: environment,
                    content: ManuscriptumScaffold(
                        start: WindowsFileVoyager(
                            syntheticRoot: '/configuration/$id/',
                            destination: configuration.root,
                            configuration: configuration,
                            installer: installer),
                        content: const SizedBox.shrink())));
          }),
      GoRoute(
          path: '/configuration/:id/:path(.*)',
          pageBuilder: (context, state) {
            if (state.location != GoRouter.of(context).location) {
              return const NoTransitionPage(child: SizedBox.shrink());
            }
            final id = state.pathParameters['id']!;
            final destination = "${state.pathParameters['path']}";
            final configuration = environment.configurations.findById(id);
            final installer =
                environment.installers[configuration.installerId]!;
            return NoTransitionPage(
                child: ManuscriptumWindow.create(
                    context: context,
                    configuration: configuration,
                    environment: environment,
                    content: ManuscriptumScaffold(
                      start: WindowsFileVoyager(
                          syntheticRoot: '/configuration/$id/',
                          destination: destination,
                          configuration: configuration,
                          installer: installer),
                      content: const SizedBox.shrink(),
                    )));
          })
    ])
  ]);
}
