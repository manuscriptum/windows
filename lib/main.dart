import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_acrylic/flutter_acrylic.dart' as flutter_acrylic;
import 'package:go_router/go_router.dart';
import 'package:manuscriptum/src/manuscriptum/environment/environemnt.dart';
import 'package:manuscriptum/src/windows/navigation/router.dart';
import 'package:ratta_supernote_file_widget/ratta_supernote_file_widget.dart';
import 'package:window_manager/window_manager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await flutter_acrylic.Window.initialize();
  // await flutter_acrylic.Window.hideWindowControls();
  // await WindowManager.instance.ensureInitialized();

  // windowManager.waitUntilReadyToShow().then((_) async {
  //   await windowManager.setTitleBarStyle(
  //     TitleBarStyle.hidden,
  //     windowButtonVisibility: false,
  //   );
  //   await windowManager.setMinimumSize(const Size(500, 600));
  //   await windowManager.show();
  //   await windowManager.setPreventClose(true);
  //   await windowManager.setSkipTaskbar(false);
  // });

  final environment = await DesktopEnvironment.create();
  final routing =
      Routing(environment: environment, rattaFileAdapter: RattaFileAdapter());

  runApp(ManuscriptumWindows(router: routing.router));
}

class ManuscriptumWindows extends StatelessWidget {
  final GoRouter router;

  const ManuscriptumWindows({super.key, required this.router});

  @override
  Widget build(BuildContext context) {
    return FluentApp.router(
      themeMode: ThemeMode.system,
      darkTheme: FluentThemeData(brightness: Brightness.dark),
      theme: FluentThemeData(brightness: Brightness.light),
      routeInformationProvider: router.routeInformationProvider,
      routerDelegate: router.routerDelegate,
      routeInformationParser: router.routeInformationParser,
    );
  }
}
